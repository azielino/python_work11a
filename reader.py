import csv
import sys
import os

input_list = sys.argv[1:]
source = input_list[0]
destination = input_list[1]
changes = input_list[2:]
dir_path = os.getcwd()

if not os.path.exists(source):
	print(f"\nBlad! Plik zrodlowy nie istnieje.\n\nDostepne pliki to: ")
	dir_files = os.listdir(path=f'{dir_path}')
	i = 0
	for file in dir_files:
		if file[-3:] == "csv":
			i += 1
			print(f" {i}) {file}")
	print("\n")
else:
	#if os.path.exists(destination):
	#	os.remove(destination)
	# input("\n   Wcisnij enter  :")
	file_rows = list()
	with open(f"{source}", newline="") as csv_file1:
		readed_csv_file1 = csv.reader(csv_file1)
		for line in readed_csv_file1:
			file_rows.append(line)
	for word in changes:
		y = int(word[0])
		x = int(word[2])
		value = word[4:]
		file_rows[y][x] = value
	print(f"\nZawartosc pliku {destination}:\n")
	with open(F"{destination}", "w", newline="") as csv_file2:
		written_csv_file2 = csv.writer(csv_file2)
		for row in file_rows:
			written_csv_file2.writerow(row)
			print(row)
	print("\n")